import os

removeEps = False

def makeEll(f):
    # Convert eps->pdf->ps->pdf (the ps->pdf part is needed to reduce the file size)
    os.system("epstopdf figs/%s.eps >figs/%s.pdf" % (f, f))
    os.system("pdf2ps figs/%s.pdf figs/%s.ps" % (f,f))
    os.system("ps2pdf -dEPSCrop figs/%s.ps figs/%s.pdf" % (f, f))
    
    # Remove the ps and (otionally) the eps
    if removeEps:
        os.system("rm figs/%s.eps" % f)
    os.system("rm figs/%s.ps" % f)
    

inFiles = [f for f in os.listdir('figs') if os.path.isfile(os.path.join('figs', f)) and ".eps" in f]

for f in inFiles:
    f = f.replace(".eps", "")
    makeEll(f)


