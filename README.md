# ell



## Getting started

Requirements: `epstopdf`, `pdf2ps`, `ps2pdf`

- Create your plots using `\ell` and save them as `.eps`
- Make a directory called `figs` and put your `.eps` files there
- Convert the `.eps` to `.pdf` including the script `\ell` with this:

```
python ellMaker.py
```

Your output will live in the `figs` directory. If you want, you can also automatically delete the `.eps` files after the conversion by setting `removeEps = True` in `ellMaker.py`

Warning: the output might not preserve any color transparency that you set :(